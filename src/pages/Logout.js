import React from "react";
import { Navigate } from "react-router-dom";

import UserContext from '../UserContext';

import { useContext, useEffect } from 'react';


export default function Logout() {
    // localStorage.clear(); // to delete localStorage value

    // Consume the UserContext object and destructure it to access the user state and unsetUser function from the context provider

    const { unsetUser, setUser } = useContext(UserContext);

    unsetUser();
    // Placing the "setUser" setter function inside of useEffect is neccessary why? because of updates within React JS that a state of another component cannot be updated while trying to render a different component

    // By adding the useEffect, this allow the Logout page to render first before triggering the useEffect which changes the state of our user.

    useEffect(() => {
        // Set the user state back to its original value
        setUser({_id: null});
    })
    return (
        <Navigate to="/login"/>
    )
};